# Trains a simple deep NN on the MNIST dataset.
from __future__ import print_function

import os

import random
import scipy.ndimage as sci_image
import scipy

import matplotlib.pyplot as plt
from keras.datasets import mnist
from keras.models import load_model
from pathlib import Path
from PIL import Image
from keras.preprocessing import image as image_lib
import numpy as np

def visualize_prediction(prediction):

    # Visualize training
    y_pos = range(0,10)
    predict_output = prediction[0]
    max = np.max(predict_output)
    num_pred = np.where(max == predict_output)[0]
    print("You've drawn the number {0}".format(num_pred))
    plt.bar(y_pos, predict_output, align='center', alpha=0.5)
    plt.xticks(y_pos)
    plt.show()

def predict_output(input_image, model):

    # Show image
    test = input_image[0][0]
    plt.imshow(test, cmap=plt.get_cmap('gray'))
    plt.show()
    model.predict_classes(input_image)
    prediction = model.predict(input_image)
    print(prediction)

    visualize_prediction(prediction)

def predict_output_mnist(input_image, model):

    # Show image
    test = input_image[0]
    plt.imshow(test, cmap=plt.get_cmap('gray'))
    plt.show()
    k = np.array(input_image)
    y = k.reshape(1, 1, 28, 28)
    model.predict_classes(y)
    prediction = model.predict(y)
    print(prediction)

    visualize_prediction(prediction)

def randomImage():
    # Load dataset
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    # Init data shapes
    x_test = x_test.reshape(x_test.shape[0], 1, 28, 28)
    x_test = x_test.astype('float32')
    x_test /= 255
    print(x_test.shape[0], 'test samples')

    # Grab random image
    rand_index = random.randrange(0, len(x_test))
    image_data = x_test[rand_index]

    return image_data

def main():

    #Init paths
    mnist = False
    trainer_path = "Image_trainer.py"
    model_path = Path('model1.h5')
    image_path = Path('out.png')
    if not image_path.exists():
        print("Can't find loaded image from Drawing demo")
        image_path = Path('testimages/testnumber_5.png')

    #Init shape
    image = Image.open(image_path).convert('L').resize((28,28))
    image = image_lib.img_to_array(image)

    image = image.transpose((2, 0, 1))
    image = image.reshape(1,1,28,28)
    image = image.astype('float32')
    #image = 1 - np.asarray(image)

    image = sci_image.gaussian_filter(image, sigma=0.5)

    plt.imshow(image[0][0], cmap=plt.get_cmap('gray'))
    plt.show()

    # returns a compiled model
    # identical to the previous one
    model = {}
    print("Selected model: {0}".format(model_path))
    if model_path.exists():
        model = load_model(model_path.name)
        print("Using model: {0}".format(model_path))
    else:
        print("Can't find model: {0}.\nCreating training model using {1}...".format(model_path, trainer_path))
        os.system(trainer_path)
        model = load_model(model_path.name)

    # Input image to model
    if(mnist):
        input_image = randomImage()  # (using random image from test set for tesing purposes)
        predict_output_mnist(input_image, model)
    else:
        input_image = image
        predict_output(input_image, model)


if __name__ == '__main__':
    main()
