# Trains a simple deep NN on the MNIST dataset.
from __future__ import print_function

# pre install comet_ml by running : pip install comet_ml
# make sure comet_ml is the first import (before all other Machine learning lib)
from comet_ml import Experiment

import matplotlib.pyplot as plt
import tensorflow as tf
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras import backend as K


def GPU_init():

    # Init GPU
    K.set_image_dim_ordering('th')
    config = tf.ConfigProto( device_count = {'GPU': 1 , 'CPU': 4} )
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    keras.backend.set_session(sess)

def visualize_training(history):
    # Plot training & validation accuracy values
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')

    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()

    # Plot training & validation loss values
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()

def main():
    num_classes = 10

    GPU_init()

    # the data, shuffled and split between train and test sets
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    #Init data shapes
    x_train = x_train.reshape(x_train.shape[0], 1, 28, 28)
    x_test = x_test.reshape(x_test.shape[0], 1, 28, 28)
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')

    # convert class vectors to binary class matrices
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    # returns a compiled model
    model, training_history = train(x_train, y_train, x_test, y_test)
    visualize_training(training_history)

def build_model_graph():
    model = Sequential()

    model.add(Convolution2D(32, 3, 3, activation='relu', input_shape=(1,28,28)))
    model.add(Convolution2D(32, 3, 3, activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

    return model

def train(x_train, y_train, x_test, y_test):
    # Define model
    model = build_model_graph()
    # Setting the API key (saved as environment variable)
    experiment = Experiment(
        api_key="y59pFNmhsWPXu8u1savZzHFAI",
        project_name="general",
        workspace="kirasm")
    experiment.log_dataset_hash(x_train)

    # and thats it... when you run your code all relevant data will be tracked and logged in https://www.comet.ml/view/y59pFNmhsWPXu8u1savZzHFAI
    training_history = model.fit(x_train, y_train, batch_size=128,
              epochs=5, validation_data=(x_test, y_test))

    score = model.evaluate(x_test, y_test, verbose=0)

    model.save('model1.h5')  # creates a HDF5 file 'model1.h5'
    return (model, training_history)

if __name__ == '__main__':
    main()
