import os
import scipy.misc

import cv2
import numpy as np

drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle to curve
ix,iy = -1,-1
classifier_path = "Image_classifier.py"
img = np.zeros((512,512,3), np.uint8)
# mouse callback function
def draw_circle(event,x,y,flags,param):
    global ix,iy,drawing,mode,img
    #print('event: {0}\nx: {1}\ny: {2}\nflags: {3}\nparams: {4}'.format(event,x,y,flags,param))
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y

    elif event == cv2.EVENT_RBUTTONUP:
        print("adad")

    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            if mode == True:
                cv2.circle(img,(x,y),11,(0,0,255),-1)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        scipy.misc.imsave('out.png', img)
        os.system(classifier_path)

cv2.namedWindow('image')
cv2.setMouseCallback('image',draw_circle)

while(1):
    cv2.imshow('image',img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27:
        break

cv2.destroyAllWindows()